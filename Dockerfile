FROM openjdk:11
VOLUME /tmp
COPY target/*.jar app.jar a /
ENTRYPOINT ["java","-jar","/app.jar"]