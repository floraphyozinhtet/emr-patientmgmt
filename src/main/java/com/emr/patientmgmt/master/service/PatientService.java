package com.emr.patientmgmt.master.service;

import com.emr.patientmgmt.master.model.Patient;

public interface PatientService {
    java.util.List<Patient> listPatients();
    Patient getPatientByMobileNumber(Integer mobileNumber);
    Patient getPatientByNric(String nric);
    Patient getPatient(Long id);
    Patient addPatient(Patient patient);
    Patient updatePatient(Long id, Patient patient);
}
