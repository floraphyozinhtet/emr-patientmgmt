package com.emr.patientmgmt.master.service;

import com.emr.patientmgmt.master.model.Patient;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PatientServiceImpl implements PatientService{
    private java.util.List<Patient> patients = new java.util.ArrayList<Patient>();

    public PatientServiceImpl(){
        patients.add(new Patient("Wong Yew Hoong", "S12312312A", new Date(), "Bukit Batok", 677989, 99909990));
        patients.add(new Patient("Allan Chong", "S12312312A", new Date(), "Bukit Batok", 677988, 99909992));
        patients.add(new Patient("Derek Lin", "S12312312A", new Date(), "Bukit Batok", 677987, 99909993));
        patients.add(new Patient("Uthara Venkatachari", "S12312312A", new Date(), "Bukit Batok", 677986, 99909994));
    }

    @Override
    public java.util.List<Patient> listPatients() {
        return patients;
    }

    @Override
    public Patient getPatientByMobileNumber(Integer mobileNumber) {
        for (Patient patient : patients) {
            if (patient.getMobileNumber().compareTo(mobileNumber) == 0) {
                return patient;
            }
        }
        return null;
    }

    @Override
    public Patient getPatientByNric(String nric) {
        for (Patient patient : patients) {
            if (patient.getNric().equals(nric)) {
                return patient;
            }
        }
        return null;
    }

    @Override
    public Patient getPatient(Long id) {
        for (Patient patient : patients) {
            if (patient.getId().compareTo(id) == 0) {
                return patient;
            }
        }
        return null;
    }


    @Override
    public Patient addPatient(Patient patient) {
        patients.add(patient);
        return patient;
    }

    @Override
    public Patient updatePatient(Long id, Patient newPatient){
        for(Patient patient : patients){
            if(patient.getId().equals(id)){
                patient.setName(newPatient.getName());
                patient.setNric(newPatient.getNric());
                patient.setDob(newPatient.getDob());
                patient.setAddress(newPatient.getAddress());
                patient.setZipcode(newPatient.getZipcode());
                patient.setMobileNumber(newPatient.getMobileNumber());
                return patient;
            }
        }

        return null;
    }
}
