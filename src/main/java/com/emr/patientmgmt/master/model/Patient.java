package com.emr.patientmgmt.master.model;

import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

public class Patient {
    private static final AtomicLong counter = new AtomicLong();

    private Long id;
    private String name;
    private String nric;
    private Date dob;
    private String address;
    private Integer zipcode;
    private Integer mobileNumber;

    public Patient(String name, String nric, Date dob, String address, Integer zipcode, Integer mobileNumber) {
        this.id = counter.incrementAndGet();
        this.name = name;
        this.nric = nric;
        this.dob = dob;
        this.address = address;
        this.zipcode = zipcode;
        this.mobileNumber = mobileNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNric() {
        return nric;
    }

    public void setNric(String nric) {
        this.nric = nric;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getZipcode() {
        return zipcode;
    }

    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }

    public Integer getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(Integer mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
