package com.emr.patientmgmt.master.controller;

import com.emr.patientmgmt.master.service.PatientService;
import com.emr.patientmgmt.master.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PatientController {

    @Autowired
    private PatientService patientService;

    /**
     * List all patients in the system
     * @return list of all patients
     */
    @RequestMapping(value="/getPatients", method = RequestMethod.GET)
    public List<Patient> getPatients(){
        return patientService.listPatients();
    }

    /**
     * Search for patient with the given mobile number
     * If there is no patient with the given mobile number, throw a PatientNotFoundException
     * @param mobileNumber
     * @return patient with the given id
     */
    @RequestMapping(value="/getPatientbyMobileNumber/{mobileNumber}", method = RequestMethod.GET)
    public Patient getPatientByMobileNumber(@PathVariable Integer mobileNumber){
        Patient patient = patientService.getPatientByMobileNumber(mobileNumber);

        // Need to handle "patient not found" error using proper HTTP status code
        // In this case it should be HTTP 404
        if(patient == null) throw new PatientNotFoundException(mobileNumber);
        return patient;

    }
    /**
     * Add a new patients with POST request to "/addPatient"
     * Note the use of @RequestBody
     * @param patient
     * @return patient
     */
    @PostMapping(value = "/createPatient", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Patient createPatient(@RequestBody Patient patient){
        if (patientService.getPatientByNric(patient.getNric()) != null) {
            throw new PatientDuplicateRecordException(patient.getNric());
        }
        if (patientService.getPatientByMobileNumber(patient.getMobileNumber()) != null) {
            throw new PatientDuplicateRecordException(patient.getMobileNumber());
        }
        return patientService.addPatient(patient);
    }

    /**
     * Update patient's info
     If there is no patient with the given "id", throw a PatientNotFoundException
     * @param id
     * @param newPatient
     * @return the updated, or newly added patient
     */
    @PostMapping(value = "/updatePatient/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Patient updatePatient(@PathVariable Long id, @RequestBody Patient newPatient){
        // Validate
        Patient existingPatient = patientService.getPatientByNric(newPatient.getNric());
        if (existingPatient != null) {
            if (existingPatient.getId().compareTo(id) != 0) {
                throw new PatientDuplicateRecordException(newPatient.getNric());
            }
        }
        existingPatient = patientService.getPatientByMobileNumber(newPatient.getMobileNumber());
        if (existingPatient != null) {
            if (existingPatient.getId().compareTo(id) != 0) {
                throw new PatientDuplicateRecordException(newPatient.getMobileNumber());
            }
        }
        // Update Patient Info
        Patient patient = patientService.updatePatient(id, newPatient);
        if(patient == null) throw new PatientNotFoundException(id);

        return patient;
    }
}
