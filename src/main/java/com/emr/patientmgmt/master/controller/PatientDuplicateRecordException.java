package com.emr.patientmgmt.master.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT) // 409 Error
public class PatientDuplicateRecordException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    PatientDuplicateRecordException(String nric) {
        super("Duplicate Record. Patient with same NRIC : " + nric + " exists. ");
    }

    PatientDuplicateRecordException(Integer mobileNumber) {
        super("Duplicate Record. Patient with same Mobile Number : " + mobileNumber  + " exists. ");
    }
}
