package com.emr.patientmgmt.master.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PatientNotFoundException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    PatientNotFoundException(Long id) {
        super("Could not find Patient with patient id : " + id);
    }

    PatientNotFoundException(Integer mobileNumber) {
        super("Could not find Patient with Mobile Number : " + mobileNumber);
    }
}
