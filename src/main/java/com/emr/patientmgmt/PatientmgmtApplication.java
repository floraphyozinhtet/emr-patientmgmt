package com.emr.patientmgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.emr.patientmgmt.master"})
public class PatientmgmtApplication {

	public static void main(String[] args) {

		SpringApplication.run(PatientmgmtApplication.class, args);
		System.out.println("CICD");
	}

}
